"""Jupyter handler for mkdocstrings."""

from mkdocstrings_handlers.jupyter.handler import get_handler

__all__ = ["get_handler"]
