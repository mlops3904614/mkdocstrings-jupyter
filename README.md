# mkdocstrings-jupyter

[![ci](https://github.com/denis-gr/mkdocstrings-jupyter/workflows/ci/badge.svg)](https://github.com/denis-gr/mkdocstrings-jupyter/actions?query=workflow%3Aci)
[![documentation](https://img.shields.io/badge/docs-mkdocs%20material-blue.svg?style=flat)](https://denis-gr.github.io/mkdocstrings-jupyter/)
[![pypi version](https://img.shields.io/pypi/v/mkdocstrings-jupyter.svg)](https://pypi.org/project/mkdocstrings-jupyter/)
[![gitpod](https://img.shields.io/badge/gitpod-workspace-blue.svg?style=flat)](https://gitpod.io/#https://github.com/denis-gr/mkdocstrings-jupyter)
[![gitter](https://badges.gitter.im/join%20chat.svg)](https://app.gitter.im/#/room/#mkdocstrings-jupyter:gitter.im)

A Jupyter handler for mkdocstrings.

## Installation

With `pip`:

```bash
pip install mkdocstrings-jupyter
```

With [`pipx`](https://github.com/pipxproject/pipx):

```bash
python3.8 -m pip install --user pipx
pipx install mkdocstrings-jupyter
```
